/*
010-035 Counter
040-072 Pizza
077-113 Salad
118-155 softdrinks
160-180 Feedback
*/


/*Counter*/

//Fügt plus 1 zum COunter hinzu
function incrementCounter() {
  const counterElement = document.getElementById('counter');
  let counter = parseInt(localStorage.getItem('counter')) || 0;
  counter++;
  localStorage.setItem('counter', counter);
  counterElement.textContent = counter;
}


//Erstellt die Meldung und setzt den Counter auf 0
function order() {
  const counterElement = document.getElementById('counter');
  localStorage.setItem('counter', 0);
  counterElement.textContent = 0;
  alert('Danke für das bestellen!');
}


document.addEventListener('DOMContentLoaded', () => {
  const counterElement = document.getElementById('counter');
  const counter = parseInt(localStorage.getItem('counter')) || 0;
  counterElement.textContent = counter;
});




//            Pizza
document.addEventListener('DOMContentLoaded', function () {
  fetch('pizzas.json') //Verbindung zum json
    .then(response => response.json())
    .then(data => {
      let html = '';

      data.forEach(pizzas => {
        html += `
        <div class="pizzaItem">
        <div class="pizzaPic">
          <img src="${pizzas.imageUrl}" alt="Pizza picture">
          </div>
          <div class="pizzaInfo">
            <h4>${pizzas.name}</h4>
            <h4 class="price">${pizzas.prize}</h4>
            <button class="cartButton" id="incrementCounter" onclick="incrementCounter()">
              <img src="Bilder/shoppingcart.png" alt="Add to Cart">
            </button>
          </div>
          <p>${pizzas.ingredients.join(', ')}</p>
        </div>
      `;
      });
        //Die Daten auf die Seite
      document.getElementById('pizzaContainer').innerHTML = html;

      console.log(data);
    })
    .catch(error => {
      console.error('Aktuell keine Pizzas vorhanden', error);
    });
});




//            Salat
document.addEventListener('DOMContentLoaded', function () {
  fetch('salads.json')  //Verbindung zum json
    .then(response => response.json())
    .then(data => {
      let html = '';

      data.forEach(salads => {
        html += `
        <div class="saladsItem">
            <img src="${salads.imageUrl}" alt="Salad picture">
            <div class="saladsInfo">
                <h4>${salads.name}</h4>
                <p>${salads.ingredients.join(', ')}</p>
                <div class="saladsControls">
                    <select>
                        <option>Italian dressing</option>
                        <option>French dressing</option>
                    </select>
                    <h4>${salads.prize}</h4>
                    <button class="cartButton" id="incrementCounter" onclick="incrementCounter()">
                        <img src="Bilder/shoppingcart.png" alt="Add to Cart">
                    </button>
                </div>
            </div>
        </div>
    `;
      });
        //Die Daten auf die Seite
      document.getElementById('saladsContainer').innerHTML = html;

      console.log(data);
    })
    .catch(error => {
      console.error('Aktuell keine Salate vorhanden', error);
    });
});




//            Softdrinks
document.addEventListener('DOMContentLoaded', function () {
  fetch('softdrinks.json')  //Verbindung zum json
    .then(response => response.json())
    .then(data => {
      let html = '';

      data.forEach(softdrinks => {
        html += `
        <div class="softdrinksItem">
            <div class="softdrinksPicture">
            <img class="softdrinks" src="${softdrinks.imageUrl}" alt="Softdrink picture">
            </div>
            
                <h3>${softdrinks.name}</h4>
                <div class="softdrinksInfo">                  
                  
                  <p>${softdrinks.volume}</p>
                  <h4>${softdrinks.prize}</h4>
                  <button class="cartButton" id="incrementCounter" onclick="incrementCounter()">
                      <img src="Bilder/shoppingcart.png" alt="Add to Cart">
                  </button>
                    
                  
                </div>
            
        </div>
    `;
      });
        //Die Daten auf die Seite
      document.getElementById('softdrinksContainer').innerHTML = html;   

      console.log(data);        
    })
    .catch(error => {
      console.error('Aktuell keine Salate vorhanden', error);
    });
});




//        Feedback
document.getElementById("feedbackForm").addEventListener("submit", function(event) {

    event.preventDefault();
    // Alle Felder ausgefüllt?
    var requiredFields = document.querySelectorAll("[required]");
    var isValid = true;
    
    for (var i = 0; i < requiredFields.length; i++) {
        if (!requiredFields[i].value) {
        isValid = false;
        var questionId = requiredFields[i].name + "requiredFields";
        var requiredFieldsMessage = document.getElementById(questionId);
        requiredFieldsMessage.innerHTML = "Please fill in this field.";
    }
}
    if (isValid) {
    //wenn alle ausgfüllt sind, gelange ich zur dankesseite
    window.location.href = "thanks.html"
    }
});